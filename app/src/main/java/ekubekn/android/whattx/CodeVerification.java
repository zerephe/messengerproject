package ekubekn.android.whattx;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class CodeVerification extends AppCompatActivity implements View.OnClickListener{

    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    private Button mCodeVerifyButton;
    private Button mResendButton;

    private EditText mVerificationCode;
    private String mPhoneNumberField;
    private String mCountryCode;
    private String mVerificationId;

    public static PhoneAuthProvider.ForceResendingToken mResendToken;
    public static PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code_verifying);

        Intent phoneVerIntent = getIntent();

        mAuth = FirebaseAuth.getInstance();

        mCodeVerifyButton = findViewById(R.id.codeVerify_button);
        mResendButton = findViewById(R.id.resend_code_button);
        mVerificationCode = findViewById(R.id.code_field);
        mPhoneNumberField = phoneVerIntent.getStringExtra("mPhoneNumberField");
        mCountryCode = phoneVerIntent.getStringExtra("mCountryCode");
        mVerificationId = phoneVerIntent.getStringExtra("mVerificationId");

        //mResendToken = phoneVerIntent.getParcelableExtra("mResendToken");

        Log.d("TOKEN", mResendToken.toString());


        mResendButton.setOnClickListener(this);
        mCodeVerifyButton.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {

        mResendToken = null;
        mCallbacks = null;

        Intent phoneVerIntent = new Intent(CodeVerification.this, PhoneVerification.class);
        phoneVerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        startActivity(phoneVerIntent);

    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SIGNING IN MESSAGE: ", "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            // [START_EXCLUDE]
                            updateUI(STATE_SIGNIN_SUCCESS, user);
                            // [END_EXCLUDE]
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("SIGNING IN MESSAGE: ", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                mVerificationCode.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                        }
                    }
                });
    }
    // [END sign_in_with_phone]

    private void updateUI(int uiState, FirebaseUser user) {
        switch (uiState) {

            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check

                break;
            case STATE_SIGNIN_SUCCESS:

                Log.d("COde verification", "111");
                try {
                    user = mAuth.getCurrentUser();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    if(user.getDisplayName() != null){
                        Log.d("CodeVerification","finally IF");
                        Intent intent = new Intent(CodeVerification.this, TabMain.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        startActivity(intent);
                    }
                    else {
                        Log.d("COdeVerification", "finally ELSE");
                        Intent intent = new Intent(CodeVerification.this, UserNameEnterActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        startActivity(intent);
                    }
                }

                break;
        }

    }

    // [START resend_verification]
    public void resendVerificationCode(String phoneNumber, PhoneAuthProvider.ForceResendingToken token, PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                callbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //If check button pressed in code_verifying.xml
            case R.id.codeVerify_button:
                String code = mVerificationCode.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationCode.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                break;

            //If RESEND button pressed in verify_send.xml
            case R.id.resend_code_button:
                Log.d("TAGTAGTAG", ("+" + mCountryCode + mPhoneNumberField));
                resendVerificationCode(("+" + mCountryCode + mPhoneNumberField), mResendToken, mCallbacks);
                break;

        }
    }
}
