package ekubekn.android.whattx;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ekubekn.android.whattx.tab_fragments.ChatItems;
import ekubekn.android.whattx.tab_fragments.ChatsFragment;
import ekubekn.android.whattx.tab_fragments.ContactItems;
import ekubekn.android.whattx.tab_fragments.ContactsFragment;
import ekubekn.android.whattx.tab_fragments.ContactsRecyclerViewAdapter;


public class GroupSettings extends AppCompatActivity {

    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    private RecyclerView mRecylerView;
    private ChatItems groupDetails;
    private List<ContactItems> mContactItems = new ArrayList<>();

    private ImageButton backButton;
    private ImageButton changeImageButton;
    private Button saveButton;
    private EditText groupName;
    private ImageView groupImage;
    String groupId;
    String groupImageId = null;


    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_settings_layout);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //obtaining group details
        groupDetails = getGroupDetails();

        for (ContactItems contact : ContactsFragment.contactItems){
            if(groupDetails.getGroupMembers().containsKey(contact.getuID())){
                mContactItems.add(0, contact);
            }
        }

        mRecylerView = findViewById(R.id.group_member_list);

        ContactsRecyclerViewAdapter contactsRecyclerViewAdapter = new ContactsRecyclerViewAdapter(GroupSettings.this, mContactItems, true);

        ItemDividerDecorator decoration = new ItemDividerDecorator(GroupSettings.this, null, false, false);
        mRecylerView.setLayoutManager(new LinearLayoutManager(GroupSettings.this));
        mRecylerView.addItemDecoration(decoration);
        mRecylerView.setAdapter(contactsRecyclerViewAdapter);

        backButton = findViewById(R.id.group_settings_back_button);
        changeImageButton = findViewById(R.id.group_settings_change_image);
        saveButton = findViewById(R.id.group_settings_save_button);
        groupImage = findViewById(R.id.group_settings_profile_image);
        groupName = findViewById(R.id.group_settings_group_title);

        groupName.setText(groupDetails.getGroupTitle());

        if (!groupDetails.getUserTwoImageId().equals("null.jpg") && groupDetails.getUserTwoImageId() != null) {

            groupImageId = groupDetails.getUserTwoImageId().substring(0, groupDetails.getUserTwoImageId().length() - 4);

            Picasso.get()
                    .load(new File(getFilesDir(), groupImageId + ".jpg"))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(200, 200)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 300x300 pixels
                    .into(groupImage);
        }
        else {
            groupImage.setImageResource(R.drawable.ic_image_sample_190dp);
        }

        groupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imageViewer = new Intent(GroupSettings.this, ImageViewer.class);
                imageViewer.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                imageViewer.putExtra("imageId", groupDetails.getUserTwoImageId());
                startActivity(imageViewer);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupSettings.super.onBackPressed();
            }
        });

        changeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(groupName.getText())){

                    mDatabase.child("groups").child(groupId).child("groupTitle").setValue(groupName.getText().toString());

                    GroupSettings.super.onBackPressed();
                }
                else {
                    groupName.setError("Group name cannot be empty!");
                }
            }
        });

    }

    private void chooseImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data != null && data.getData() != null )
        {

            filePath = data.getData();

            mDatabase.child("groups").child(groupId).child("userTwoImageId").removeValue();
            mDatabase.child("groups").child(groupId).child("userTwoImageId").setValue(groupImageId + ".jpg");

            //setting image into imageView
            Picasso.get()
                    .load(filePath)
                    .resize(600, 600)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                    .into(new ImageWriter(getApplicationContext(), groupImageId, groupImage));


        }


    }

    ChatItems getGroupDetails(){

        Gson gson = new Gson();
        Intent intent = getIntent();
        groupId = intent.getStringExtra("groupId");

        String jsonString = IOClass.readFromFile(GroupSettings.this, "groups.json");
        Type chatTypeList = new TypeToken<ArrayList<ChatItems>>() {
        }.getType();
        List<ChatItems> groups = gson.fromJson(jsonString, chatTypeList);

        for(ChatItems group : groups){
            if(group.getGroupId().equals(groupId)){
                return group;
            }
        }
        return null;
    }
}
