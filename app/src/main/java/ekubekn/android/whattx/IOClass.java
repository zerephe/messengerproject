package ekubekn.android.whattx;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ekubekn.android.whattx.tab_fragments.ChatsFragment;
import ekubekn.android.whattx.tab_fragments.ContactsFragment;

public class IOClass {

    public static boolean callback = false;

    public static void uploadImage(final Context context, StorageReference storageReference, final String imageId) {

        UploadTask uploadTask;
            try {
                uploadTask = storageReference.putStream(new FileInputStream(new File(context.getFilesDir(), imageId + ".jpg")));
                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Toast.makeText(context, "Uploaded", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(context, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
    }

    public static void downloadImage(Context context, String name){

        callback = false;
        name = name.substring(0, name.length() - 4);

        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("images/" + name);

        File file = new File(context.getFilesDir(), name + ".jpg");

        storageReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                callback = true;
                ChatsFragment.chatsRecyclerViewAdapter.notifyDataSetChanged();
                ContactsFragment.contactsRecyclerViewAdapter.notifyDataSetChanged();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback = false;
            }
        });

    }

    public static String readFromFile(Context context, String fileName){
        try {
            FileInputStream fis = new FileInputStream(new File(context.getFilesDir(), fileName));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();

            return new String(buffer);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeToFile(Context context, String fileName, String text){

        try {
            FileOutputStream fos;
            fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fos.write(text.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
