package ekubekn.android.whattx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ImageViewer extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView imageView;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        TabMain.imageViewFlag = true;

        //setting toolbar title
        toolbar = findViewById(R.id.image_viewer_toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        imageView = findViewById(R.id.image_viewer_view);
        backButton = findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageViewer.super.onBackPressed();
            }
        });

        Intent intent = getIntent();
        String imageId = intent.getStringExtra("imageId");
        if(imageId != null){
            Picasso.get()
                    .load(new File(getFilesDir(), imageId))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(600, 600)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                    .into(imageView);
        }
        else {
            Toast.makeText(this, "No image!", Toast.LENGTH_SHORT).show();
        }
    }
}
