package ekubekn.android.whattx;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageWriter implements Target {

    private final String name;
    private ImageView imageView;

    StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private Context context;


    public ImageWriter(Context context, String name, ImageView imageView) {
        this.name = name;
        this.imageView = imageView;
        this.context = context;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

        try {
            FileOutputStream outputStream = context.openFileOutput( name + ".jpg", Context.MODE_PRIVATE);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
            outputStream.close();

            imageView.setImageBitmap(bitmap);

            //uploading image
            IOClass.uploadImage(
                    context,
                    storageReference.child("images/" + name),
                    name);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }
}
