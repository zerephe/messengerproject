package ekubekn.android.whattx;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.UUID;

import ekubekn.android.whattx.group_chat.GroupFragment;
import ekubekn.android.whattx.tab_fragments.ChatsFragment;
import ekubekn.android.whattx.tab_fragments.ContactsRecyclerViewAdapter;

public class NewGroupChatActivity extends AppCompatActivity {

    private static FirebaseUser currentUser;
    private static FirebaseAuth mAuth;
    private static DatabaseReference mDatabase, groupsRef;

    private final int PICK_IMAGE_REQUEST = 71;

    private static EditText groupName;
    private static Button enterGroupName;
    private ImageView groupImage;
    private Uri filePath;

    static String groupChatId;
    private static String imageId = null;

    @Override
    public void onBackPressed() {

        ContactsRecyclerViewAdapter.deselectAllContacts();
        TabMain.newGroupButton.setVisibility(View.INVISIBLE);
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_name_set_layout);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        groupsRef = mDatabase.child("groups");

        enterGroupName = findViewById(R.id.group_name_enter_button);
        groupName = findViewById(R.id.group_name_textedit);
        groupImage = findViewById(R.id.group_image);

        //setting group image

        groupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        enterGroupName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(groupName.getText().toString())){

                    ContactsRecyclerViewAdapter.mUserSelector.put(
                            currentUser.getUid(),
                            currentUser.getDisplayName()
                    );

                    groupChatId = ChatsFragment.newGroupChat(
                            getApplicationContext(),
                            groupName.getText().toString(),
                            ContactsRecyclerViewAdapter.mUserSelector,
                            "",
                            imageId + ".jpg");

                    Intent newChat = new Intent(getApplicationContext(), GroupFragment.class);
                    newChat.putExtra("chatId", groupChatId);
                    newChat.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newChat);
                    finish();
                }
                else{
                    groupName.setError("Cannot be empty!");
                }
            }
        });
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        imageId = UUID.randomUUID().toString();

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data != null && data.getData() != null )
        {

            filePath = data.getData();

            //setting image into imageView
            Picasso.get()
                    .load(filePath)
                    .resize(600, 600)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                    .into(new ImageWriter(getApplicationContext(), imageId, groupImage));


//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//                double width=bitmap.getWidth();
//                double height=bitmap.getHeight();
//                double ratio=1;
//                if(height>width){
//                    ratio=height/width;
//                    width=220/ratio;
//                    height=220;
//                }
//                else{
//                    ratio=width/height;
//                    height=220/ratio;
//                    width=220;
//                }
//                Bitmap resized = Bitmap.createScaledBitmap(bitmap, (int)width, (int)height, true);
//                groupImage.setImageBitmap(resized);
//            }
//            catch (IOException e)
//            {
//                e.printStackTrace();
//            }
        }
    }

}
