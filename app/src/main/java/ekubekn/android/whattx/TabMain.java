package ekubekn.android.whattx;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ekubekn.android.whattx.tab_fragments.ChatsFragment;
import ekubekn.android.whattx.tab_fragments.ContactsFragment;
import ekubekn.android.whattx.tab_fragments.ContactsRecyclerViewAdapter;

public class TabMain extends AppCompatActivity{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;

    public static boolean imageViewFlag = false;

    public static FloatingActionButton newGroupButton;

    private static FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        checkState();

        setContentView(R.layout.activity_tab_main);

        newGroupButton = findViewById(R.id.new_group_button);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mSectionsPagerAdapter.addFragment(new ChatsFragment(), "Chats");
        mSectionsPagerAdapter.addFragment(new ContactsFragment(), "Contacts");
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.getTabAt(0).setIcon(R.drawable.chat_icon_24x24);
        tabLayout.getTabAt(1).setIcon(R.drawable.contacts_icon_24x24);


        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    newGroupButton.setVisibility(View.INVISIBLE);
                }
                else if(ContactsRecyclerViewAdapter.selectUser){
                    newGroupButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public static Long getCurrentTimestamp(){
        Date date = new Date();

        return date.getTime();
    }

    @Override
    public void onBackPressed() {

        if(mViewPager.getCurrentItem() == 1 && ContactsRecyclerViewAdapter.selectUser){
            ContactsRecyclerViewAdapter.deselectAllContacts();
            newGroupButton.setVisibility(View.INVISIBLE);
        }
        else{
            if(mViewPager.getCurrentItem() > 0){
                mViewPager.setCurrentItem(0);
            }
            else{
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        if(!imageViewFlag){
            mViewPager.setCurrentItem(0, false);
        }
        else {
            imageViewFlag = false;
        }
        super.onStart();
    }

    //checking if user is loged in
    public void checkState(){
        if(mAuth.getCurrentUser() != null){
            //pass
        }
        else{
            logOut();
        }

    }

    public void logOut(){
        Log.d("MAIN", "secocnd");
        Intent intent = new Intent(TabMain.this, PhoneVerification.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        startActivity(intent);
        overridePendingTransition(0, 0);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.log_out_opt){
            mAuth.signOut();
            CodeVerification.mCallbacks = null;
            Intent intent = new Intent(TabMain.this, PhoneVerification.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
            finish();
            startActivity(intent);

        }
        else if(id == R.id.profile_settings_opt){

            Intent intent = new Intent(TabMain.this, ProfileSettings.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentListTitles = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return fragmentListTitles.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentListTitles.get(position);
        }

        public void addFragment(Fragment fragment, String title){

            fragmentList.add(fragment);
            fragmentListTitles.add(title);

        }
    }
}
