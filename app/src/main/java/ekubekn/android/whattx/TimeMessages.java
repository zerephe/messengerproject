package ekubekn.android.whattx;

import ekubekn.android.whattx.single_chat.MessageItems;

public class TimeMessages {

    private String id;
    private MessageItems messageItems;

    public TimeMessages() {
    }

    public TimeMessages(String id, MessageItems messageItems) {
        this.id = id;
        this.messageItems = messageItems;
    }

    //setters
    public void setId(String id) {
        this.id = id;
    }

    public void setMessageItems(MessageItems messageItems) {
        this.messageItems = messageItems;
    }

    //getters
    public String getId() {
        return id;
    }

    public MessageItems getMessageItems() {
        return messageItems;
    }
}
