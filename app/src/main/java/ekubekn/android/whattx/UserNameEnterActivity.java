package ekubekn.android.whattx;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.UUID;


public class UserNameEnterActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int PICK_IMAGE_REQUEST = 71;
    private Button mUNameSubmitButton;
    private EditText mUserName;
    private ImageView profileImage;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabase;
    private StorageReference storageReference;

    private static String imageId = null;
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.name_input_layout);

        mUserName = findViewById(R.id.userName_field);
        profileImage = findViewById(R.id.profile_image);

        mUNameSubmitButton = findViewById(R.id.uName_submit_button);
        mUNameSubmitButton.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        storageReference = FirebaseStorage.getInstance().getReference();

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
    }

    @Override
    public void onBackPressed() {

        mAuth.signOut();

        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            //If NEXT button pressed in verify_send.xml
            case R.id.uName_submit_button:

                if(mUserName.getText() == null){
                    mUserName.setError("Invalid user name!");
                }
                else{
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(mUserName.getText().toString())
                            .build();

                    currentUser.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("USERNAME ENTER ACTIVITY", "User profile updated.");
                                    }
                                }
                            });
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {

                        //uploading image
                        IOClass.uploadImage(
                                getApplicationContext(),
                                storageReference.child("images/" + imageId),
                                imageId);

                        mDatabase.child("users").child(currentUser.getUid().toString()).child("username").setValue(mUserName.getText().toString());
                        mDatabase.child("users").child(currentUser.getUid().toString()).child("phoneNumber").setValue(currentUser.getPhoneNumber().toString());
                        mDatabase.child("users").child(currentUser.getUid().toString()).child("profileImage").setValue(imageId + ".jpg");

                        Intent intent = new Intent(UserNameEnterActivity.this, TabMain.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        startActivity(intent);
                    }
                }
                break;
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        imageId = currentUser.getUid();

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data != null && data.getData() != null) {

            filePath = data.getData();

            //setting image into imageView
            Picasso.get()
                    .load(filePath)
                    .resize(600, 600)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                    .into(new ImageWriter(getApplicationContext(), imageId, profileImage));

        }
    }

}
