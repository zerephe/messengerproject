package ekubekn.android.whattx.group_chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.R;
import ekubekn.android.whattx.single_chat.MessageItems;


/**
 * Created by reale on 2/28/2017.
 */

public class GroupMessagesRecyclerViewAdapter extends RecyclerView.Adapter<GroupMessagesRecyclerViewAdapter.GroupMessageViewHolder> {

    private List<MessageItems> messages;
    public Context mContext;

    public GroupMessagesRecyclerViewAdapter(Context mContext, List<MessageItems> messages) {
        this.mContext = mContext;
        this.messages = messages;
    }

    @Override
    public GroupMessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_item_message, parent, false);
        final GroupMessageViewHolder groupMessageViewHolder = new GroupMessageViewHolder(view);

        final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        groupMessageViewHolder.otherLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groupMessageViewHolder.groupMessageTimeOther.getVisibility() == View.INVISIBLE){
                    groupMessageViewHolder.groupMessageTimeOther.setVisibility(View.VISIBLE);
                    lp.setMargins(80, 0, 0, 0);
                    lp.addRule(RelativeLayout.BELOW, groupMessageViewHolder.otherLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_START);
                    groupMessageViewHolder.groupMessageTimeOther.setLayoutParams(lp);
                }
                else{
                    groupMessageViewHolder.groupMessageTimeOther.setVisibility(View.INVISIBLE);
                    lp.setMargins(80, -40, 0, 0);
                    lp.addRule(RelativeLayout.BELOW, groupMessageViewHolder.otherLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_START);
                    groupMessageViewHolder.groupMessageTimeOther.setLayoutParams(lp);
                }
            }
        });

        groupMessageViewHolder.ownerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groupMessageViewHolder.groupMessageTimeYour.getVisibility() == View.INVISIBLE){
                    groupMessageViewHolder.groupMessageTimeYour.setVisibility(View.VISIBLE);
                    lp.setMargins(0, 0, 50, 0);
                    lp.addRule(RelativeLayout.BELOW, groupMessageViewHolder.ownerLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_END);
                    groupMessageViewHolder.groupMessageTimeYour.setLayoutParams(lp);
                }
                else{
                    groupMessageViewHolder.groupMessageTimeYour.setVisibility(View.INVISIBLE);
                    lp.setMargins(0, -40, 50, 0);
                    lp.addRule(RelativeLayout.BELOW, groupMessageViewHolder.ownerLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_END);
                    groupMessageViewHolder.groupMessageTimeYour.setLayoutParams(lp);
                }
            }
        });

        return groupMessageViewHolder;
    }

    @Override
    public void onBindViewHolder(GroupMessageViewHolder holder, int position) {

        MessageItems currentMessage = messages.get(position);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, HH:mm");
        String dateString = formatter.format(new Date(currentMessage.getTimeStamp()));

        final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                10);
        final RelativeLayout.LayoutParams dp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        dp.setMargins(10, 10, 0 ,0);

        if(currentMessage.isOwner()){
            if(currentMessage.isRead()){
                holder.messageCheck.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_circle24dp));
            }
            else{
                holder.messageCheck.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_circle_dark_24dp));
            }
            holder.ownerLayout.setVisibility(View.VISIBLE);
            holder.otherLayout.setVisibility(View.INVISIBLE);
            holder.otherLayout.setLayoutParams(lp);
            holder.messageCheck.setVisibility(View.VISIBLE);
            holder.groupMessageTimeYour.setText(dateString.toUpperCase());
        }
        else {
            holder.otherLayout.setLayoutParams(dp);
            holder.messageCheck.setVisibility(View.INVISIBLE);
            holder.ownerLayout.setVisibility(View.INVISIBLE);
            holder.otherLayout.setVisibility(View.VISIBLE);
            holder.groupMessageTimeOther.setText(dateString.toUpperCase());

            if(currentMessage.getSenderImageId() != null){

                if(!new File(mContext.getFilesDir(), currentMessage.getSenderImageId()).exists()){
                    IOClass.downloadImage(mContext, currentMessage.getSenderImageId());
                }

                Picasso.get()
                        .load(new File(mContext.getFilesDir(), currentMessage.getSenderImageId()))
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .resize(60, 60)
                        .centerCrop()
                        .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                        .into(holder.userImage);
            }
            else{
                holder.userImage.setImageResource(R.drawable.ic_profile_image_sample_24dp);
            }
        }

        holder.otherName.setText(currentMessage.getSenderName());

        holder.messageTextYour.setText(currentMessage.getMessageText());
        holder.messageTextOther.setText(currentMessage.getMessageText());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class GroupMessageViewHolder extends RecyclerView.ViewHolder{

        public TextView messageTextYour, groupMessageTimeYour;
        public TextView messageTextOther, groupMessageTimeOther, otherName;
        public ImageView messageCheck;
        public ImageView userImage;
        public RelativeLayout otherLayout;
        public RelativeLayout ownerLayout;

        public GroupMessageViewHolder(View itemView) {
            super(itemView);

            messageTextYour = itemView.findViewById(R.id.message_text_your);
            messageTextOther = itemView.findViewById(R.id.message_text_other);
            messageCheck = itemView.findViewById(R.id.message_check);
            userImage = itemView.findViewById(R.id.user_image);
            otherLayout = itemView.findViewById(R.id.other_group_layout);
            ownerLayout = itemView.findViewById(R.id.owner_group_layout);
            groupMessageTimeYour = itemView.findViewById(R.id.owner_message_time);
            groupMessageTimeOther = itemView.findViewById(R.id.other_message_time);
            otherName = itemView.findViewById(R.id.other_name);
        }
    }

}