package ekubekn.android.whattx.single_chat;

/**
 * Created by Bekn on 26-Mar-18.
 */

public class MessageItems {

    private String messageText, senderId, senderName, messageId;
    private long timeStamp;
    private String deliverTime;
    private String destructTime;
    private String senderImageId;
    //private int userImage;
    private boolean owner;
    private boolean read;


    public MessageItems() {
    }

    public MessageItems(String messageText, String senderId, String senderName, String senderImageId, long timeStamp, String deliverTime, String destructTime, boolean owner, boolean read) {
        this.messageText = messageText;
        this.senderId = senderId;
        this.senderName = senderName;
        this.timeStamp = timeStamp;
        this.owner = owner;
        this.read = read;
        this.senderImageId = senderImageId;
        this.deliverTime = deliverTime;
        this.destructTime = destructTime;
    }

    public MessageItems(String messageText, String senderId, String senderImageId, long timeStamp, String deliverTime, String destructTime, boolean owner, boolean read) {
        this.messageText = messageText;
        this.timeStamp = timeStamp;
        //this.userImage = userImage;
        this.owner = owner;
        this.senderId = senderId;
        this.read = read;
        this.senderImageId = senderImageId;
        this.deliverTime = deliverTime;
        this.destructTime = destructTime;
    }

    //getters

    public boolean isRead() {
        return read;
    }

    public String getMessageText() {
        return messageText;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getSenderImageId() {
        return senderImageId;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getSenderName() {
        return senderName;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getDeliverTime() {
        return deliverTime;
    }

    public String getDestructTime() {
        return destructTime;
    }

    //setters

    public void setSenderImageId(String senderImageId) {
        this.senderImageId = senderImageId;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setDeliverTime(String deliverTime) {
        this.deliverTime = deliverTime;
    }

    public void setDestructTime(String destructTime) {
        this.destructTime = destructTime;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean isOwner(){
        return owner;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }
}
