package ekubekn.android.whattx.single_chat;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import ekubekn.android.whattx.TabMain;
import ekubekn.android.whattx.tab_fragments.ChatItems;
import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.R;
import ekubekn.android.whattx.tab_fragments.ContactsFragment;


public class MessagesFragment extends AppCompatActivity {

    private RecyclerView mRecylerView;
    private LinearLayoutManager linearLayoutManager;
    private List<MessageItems> messageItems = new ArrayList<>();
    MessagesRecyclerViewAdapter messagesRecyclerViewAdapter;
    static View view;

    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase, messagesRef;

    TimePicker timePicker;
    DatePicker datePicker;
    private Dialog timeDialog, dateDialog;
    private ImageButton sendMessageButton,timePickerButton;
    private EditText messageTextEdit;
    private EditText deliverTimeText, destructTimeText;
    private LinearLayout timeLayot;
    private Toolbar toolbar;

    private String chatID;
    private boolean flagTime = true;
    private String deliverTime = null, destructTime = null;

    ChildEventListener messageListener;

    public MessagesFragment() {
    }

    @Override
    public void onBackPressed() {

        messagesRef.child(chatID).removeEventListener(messageListener);
        super.onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_messages);

        toolbar = findViewById(R.id.chat_toolbar);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        messagesRef = mDatabase.child("messages");

        chatID = getChatId();

        messagesRecyclerViewAdapter = new MessagesRecyclerViewAdapter(getApplicationContext(), messageItems);
        mRecylerView = findViewById(R.id.messages_recycler_view);
        linearLayoutManager = new LinearLayoutManager(this);
        //linearLayoutManager.setStackFromEnd(true);

        mRecylerView.setHasFixedSize(true);
        mRecylerView.setLayoutManager(linearLayoutManager);

        sendMessageButton = findViewById(R.id.message_send_button);
        messageTextEdit = findViewById(R.id.message_send_line);
        timePickerButton = findViewById(R.id.time_picker_button);
        mRecylerView.setAdapter(messagesRecyclerViewAdapter);
        timeLayot = findViewById(R.id.time_layout);
        deliverTimeText = findViewById(R.id.deliver_time);
        destructTimeText = findViewById(R.id.destruction_time);

        deliverTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateDelay("deliverTime");
            }
        });
        destructTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getDateDelay("destructTime");
            }
        });



        timePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleTimeButton();
            }
        });
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });


        messageHandle();
        //getLocalJson();
    }

    private void getDateDelay(final String timeType) {

        dateDialog = new Dialog(MessagesFragment.this);
        dateDialog.setContentView(R.layout.date_picker);
        dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        datePicker = dateDialog.findViewById(R.id.date_Picker);
        datePicker.setMinDate(TabMain.getCurrentTimestamp());

        Button ok2Btn = dateDialog.findViewById(R.id.ok2Btn);

        ok2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getTimeDelay(timeType, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                dateDialog.hide();
            }
        });

        dateDialog.show();
    }

    private void getTimeDelay(final String timeType, final int year, final int month, final int day) {

        timeDialog = new Dialog(MessagesFragment.this);
        timeDialog.setContentView(R.layout.time_picker);
        timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        timePicker = timeDialog.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        Button okBtn = timeDialog.findViewById(R.id.submit_time_button);
        okBtn.setClickable(true);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hour = timePicker.getCurrentHour();
                int minute = timePicker.getCurrentMinute();
                System.out.println(hour + "  " + minute);

                Calendar calendar = new GregorianCalendar(year, month, day, hour, minute, 0);

                SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, HH:mm");
                String dateString = formatter.format(new Date(calendar.getTimeInMillis()));
                System.out.println("Time Delay Checking" + dateString);

                if(TabMain.getCurrentTimestamp() >= calendar.getTimeInMillis()){
                    Toast.makeText(MessagesFragment.this, "Time choose cannot be equal or less than current time!" , Toast.LENGTH_SHORT).show();
                }
                else{
                    //setting deliver or destruct time
                    if(timeType.equals("deliverTime")){
                        Long Time = calendar.getTimeInMillis();
                        Time = Time - TabMain.getCurrentTimestamp();
                        deliverTime = Time.toString();
                        deliverTimeText.setText(dateString);
                    }
                    else{
                        Long Time = calendar.getTimeInMillis();
                        Time = Time - TabMain.getCurrentTimestamp();
                        destructTime = Time.toString();
                        destructTimeText.setText(dateString);
                    }
                    timeDialog.hide();
                }

            }
        });
        timeDialog.show();
    }




    private void toggleTimeButton() {
        if(flagTime){
            timeLayot.setVisibility(View.VISIBLE);
            flagTime = false;
        }
        else {
            timeLayot.setVisibility(View.INVISIBLE);
            destructTime = null;
            deliverTime = null;
            deliverTimeText.setText("Delivering Time");
            destructTimeText.setText("Destruction Time");
            flagTime = true;
        }
    }

    public void messageHandle(){

        messageListener = new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                MessageItems message = dataSnapshot.getValue(MessageItems.class);

                if(currentUser.getUid().toString().equals(message.getSenderId())){
                    message.setOwner(true);
                    messagesRef.child(chatID).child(dataSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            if(snapshot.child("read").getValue() != null){
                                String isRead = snapshot.child("read").getValue().toString();
                                if(isRead == "true"){
                                    for(int i = 0; i < messageItems.size(); i++){
                                        if(messageItems.get(i).getMessageId() == snapshot.getKey()){
                                            messageItems.get(i).setRead(true);
                                            break;
                                        }
                                    }
                                    messagesRecyclerViewAdapter.notifyDataSetChanged();
                                    messagesRef.child(chatID).child(dataSnapshot.getKey()).removeEventListener(this);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    message.setMessageId(dataSnapshot.getKey().toString());
                    messageItems.add(message);

                    mRecylerView.scrollToPosition(messageItems.size() - 1);
                    messagesRecyclerViewAdapter.notifyDataSetChanged();

                }
                else {
                    if(message.getDeliverTime() == null){
                        message.setRead(true);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("read", true);
                        messagesRef.child(chatID).child(dataSnapshot.getKey()).updateChildren(map);
                        message.setOwner(false);

                        message.setMessageId(dataSnapshot.getKey().toString());
                        messageItems.add(message);

                        mRecylerView.scrollToPosition(messageItems.size() - 1);
                        messagesRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                int i = 0;
                for(MessageItems message : messageItems){
                    if(message.getMessageId().equals(dataSnapshot.getKey())){
                        messageItems.remove(i);
                        break;
                    }
                    i++;
                }
                messagesRecyclerViewAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        messagesRef.child(chatID).addChildEventListener(messageListener);

    }

    public String getChatId(){

        Intent intent = getIntent();
        Gson gson = new Gson();
        String jsonString = IOClass.readFromFile(getApplicationContext(), "chats.json");
        Type chatTypeList = new TypeToken<ArrayList<ChatItems>>(){}.getType();
        List<ChatItems> chats = gson.fromJson(jsonString, chatTypeList);

        String userTwo = intent.getStringExtra("userTwo");
        String chatID = intent.getStringExtra("chatID");

        if(chatID == null){
            for(int i = 0; i < chats.size(); i++){
                if(chats.get(i).getUserOne().equals(currentUser.getUid().toString()) && chats.get(i).getUserTwo().equals(userTwo)){
                    //setting toolbar title
                    toolbar.setTitle(chats.get(i).getUserTwoName());
                    setSupportActionBar(toolbar);
                    return chats.get(i).getChatId();
                }
            }
        }
        else {
            //setting toolbar title
            toolbar.setTitle(userTwo);
            setSupportActionBar(toolbar);
            return chatID;
        }

        return null;
    }
    public void sendMessage(){

        if(!TextUtils.isEmpty(messageTextEdit.getText().toString())) {
//            Map<String, MessageItems> message = new HashMap<>();
//            message.put("m" + messageItems.size(), new MessageItems(messageTextEdit.getText().toString(),
//                    currentUser.getUid().toString(), true));

            String pushId = messagesRef.child(chatID).push().getKey();

            HashMap<String,Object> timeStamp = new HashMap<>();
            timeStamp.put("timeStamp", ServerValue.TIMESTAMP);

            messagesRef.child(chatID).child(pushId).setValue(new MessageItems(
                    messageTextEdit.getText().toString(),
                    currentUser.getUid(),
                    ContactsFragment.currentUserImageId,
                    TabMain.getCurrentTimestamp(),
                    deliverTime,
                    destructTime,
                    true,
                    false));
            messagesRef.child(chatID).child(pushId).updateChildren(timeStamp);

            //nullifying time
            deliverTime = null;
            destructTime = null;
            timeLayot.setVisibility(View.INVISIBLE);
            deliverTimeText.setText("Delivering Time");
            destructTimeText.setText("Destruction Time");

        }
        else{
            messageTextEdit.setError("Invalid text message!");
        }

        messageTextEdit.setText("");
        messagesRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat_settings, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.clear_chat){

            messagesRef.child(chatID).setValue(null);

        }

        return super.onOptionsItemSelected(item);
    }

}
