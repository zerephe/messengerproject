package ekubekn.android.whattx.single_chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.R;


/**
 * Created by reale on 2/28/2017.
 */

public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.MessageViewHolder> {

    private List<MessageItems> messages;
    public Context mContext;

    public MessagesRecyclerViewAdapter(Context mContext, List<MessageItems> messages) {
        this.mContext = mContext;
        this.messages = messages;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        final MessageViewHolder messageViewHolder = new MessageViewHolder(view);

        final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        messageViewHolder.otherLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(messageViewHolder.messageTimeOther.getVisibility() == View.INVISIBLE){
                    messageViewHolder.messageTimeOther.setVisibility(View.VISIBLE);
                    lp.setMargins(90, 0, 0, 0);
                    lp.addRule(RelativeLayout.BELOW, messageViewHolder.otherLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_START);
                    messageViewHolder.messageTimeOther.setLayoutParams(lp);
                }
                else{
                    messageViewHolder.messageTimeOther.setVisibility(View.INVISIBLE);
                    lp.setMargins(90, -40, 0, 0);
                    lp.addRule(RelativeLayout.BELOW, messageViewHolder.otherLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_START);
                    messageViewHolder.messageTimeOther.setLayoutParams(lp);
                }
            }
        });

        messageViewHolder.ownerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(messageViewHolder.messageTimeYour.getVisibility() == View.INVISIBLE){
                    messageViewHolder.messageTimeYour.setVisibility(View.VISIBLE);
                    lp.setMargins(0, 0, 50, 0);
                    lp.addRule(RelativeLayout.BELOW, messageViewHolder.ownerLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_END);
                    messageViewHolder.messageTimeYour.setLayoutParams(lp);
                }
                else{
                    messageViewHolder.messageTimeYour.setVisibility(View.INVISIBLE);
                    lp.setMargins(0, -40, 50, 0);
                    lp.addRule(RelativeLayout.BELOW, messageViewHolder.ownerLayout.getId());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_END);
                    messageViewHolder.messageTimeYour.setLayoutParams(lp);
                }
            }
        });

        return messageViewHolder;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {

        MessageItems currentMessage = messages.get(position);

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, HH:mm");
        String dateString = formatter.format(new Date(currentMessage.getTimeStamp()));

        if(currentMessage.isOwner()){
            if(currentMessage.isRead()){
                holder.messageCheck.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_circle24dp));
            }
            else{
                holder.messageCheck.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_circle_dark_24dp));
            }
            holder.ownerLayout.setVisibility(View.VISIBLE);
            holder.otherLayout.setVisibility(View.INVISIBLE);
            holder.messageCheck.setVisibility(View.VISIBLE);
            holder.messageTimeYour.setText(dateString.toUpperCase());
        }
        else {
            holder.messageCheck.setVisibility(View.INVISIBLE);
            holder.ownerLayout.setVisibility(View.INVISIBLE);
            holder.otherLayout.setVisibility(View.VISIBLE);
            holder.messageTimeOther.setText(dateString.toUpperCase());

            if(currentMessage.getSenderImageId() != null){
                if(!new File(mContext.getFilesDir(), currentMessage.getSenderImageId()).exists()){
                    IOClass.downloadImage(mContext, currentMessage.getSenderImageId());
                }
                Picasso.get()
                        .load(new File(mContext.getFilesDir(), currentMessage.getSenderImageId()))
                        .resize(60, 60)
                        .centerCrop()
                        .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                        .into(holder.userImage);
            }
            else{
                holder.userImage.setImageResource(R.drawable.ic_profile_image_sample_24dp);
            }
        }

        holder.messageTextYour.setText(currentMessage.getMessageText());
        holder.messageTextOther.setText(currentMessage.getMessageText());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder{

        public TextView messageTextYour, messageTimeYour;
        public TextView messageTextOther, messageTimeOther;
        public ImageView messageCheck;
        public ImageView userImage;
        public RelativeLayout otherLayout;
        public RelativeLayout ownerLayout;

        public MessageViewHolder(View itemView) {
            super(itemView);

            messageTextYour = itemView.findViewById(R.id.message_text_your);
            messageTextOther = itemView.findViewById(R.id.message_text_other);
            messageCheck = itemView.findViewById(R.id.message_check);
            userImage = itemView.findViewById(R.id.user_image);
            otherLayout = itemView.findViewById(R.id.other_layout);
            ownerLayout = itemView.findViewById(R.id.owner_layout);
            messageTimeYour = itemView.findViewById(R.id.owner_message_time);
            messageTimeOther = itemView.findViewById(R.id.other_message_time);
        }
    }

}