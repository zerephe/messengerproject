package ekubekn.android.whattx.tab_fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//public class ChatItems{
//
//    List<Items> items = new ArrayList();
//
//}

public class ChatItems {

    //single chat vars
    private String userOne, userTwo;
    private String userOneName, userTwoName;
    private String chatId, userTwoImageId;

    //group chat vars
    private String groupTitle;
    private HashMap<String, String> groupMembers = new HashMap<>();
    private String groupId;

    public ChatItems() {
    }

    //group constructor
    public ChatItems(String groupTitle, HashMap<String, String> groupMembers, String groupId, String userTwoImageId) {
        this.groupTitle = groupTitle;
        this.groupMembers = groupMembers;
        this.groupId = groupId;
        this.userTwoImageId = userTwoImageId;
    }

    //single chat constructor
    public ChatItems(String userOneName, String userOne, String userTwoName, String userTwoImageId, String userTwo, String chatId) {
        this.userOne = userOne;
        this.userTwo = userTwo;
        this.chatId = chatId;
        this.userOneName = userOneName;
        this.userTwoName = userTwoName;
        this.userTwoImageId = userTwoImageId;
    }

    //setters

    public void setUserTwoImageId(String userTwoImageId) {
        this.userTwoImageId = userTwoImageId;
    }

    public void setUserOne(String userOne) {
        this.userOne = userOne;
    }

    public void setUserTwo(String userTwo) {
        this.userTwo = userTwo;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void setUserOneName(String userOneName) {
        this.userOneName = userOneName;
    }

    public void setUserTwoName(String userTwoName) {
        this.userTwoName = userTwoName;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public void setGroupMembers(HashMap<String, String> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    //getters

    public String getUserTwoImageId() {
        return userTwoImageId;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public HashMap<String, String> getGroupMembers() {
        return groupMembers;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserOneName() {
        return userOneName;
    }

    public String getUserTwoName() {
        return userTwoName;
    }

    public String getUserOne() {
        return userOne;
    }

    public String getUserTwo() {
        return userTwo;
    }

    public String getChatId() {
        return chatId;
    }
}
