package ekubekn.android.whattx.tab_fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.ItemDividerDecorator;
import ekubekn.android.whattx.NewGroupChatActivity;
import ekubekn.android.whattx.R;
import ekubekn.android.whattx.TabMain;
import ekubekn.android.whattx.TimeMessages;
import ekubekn.android.whattx.group_chat.GroupFragment;
import ekubekn.android.whattx.single_chat.MessageItems;

/**
 * Created by Bekn on 22-Mar-18.
 */


public class ChatsFragment extends Fragment {

    private RecyclerView mRecylerView;
    public static ChatsRecyclerViewAdapter chatsRecyclerViewAdapter;
    public static List<ContactItems> chatItems;
    public static List<String> checkers;
    public static HashMap<HashMap<String , String>, String> fireCheckers = new HashMap<>();
    public static List<TimeMessages> timeMessages = new ArrayList<>();

    private static DatabaseReference mDatabase;
    public static DatabaseReference chatsRef, groupsRef;
    public static FirebaseUser currentUser;
    private FirebaseAuth mAuth;

    View view;
    public ChatsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        chatsRef = mDatabase.child("chats");
        groupsRef = mDatabase.child("groups");

        chatItems = new ArrayList<>();
        checkers = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_chats, container, false);
        mRecylerView = (RecyclerView) view.findViewById(R.id.chats_recycler_view);

        chatsRecyclerViewAdapter  = new ChatsRecyclerViewAdapter(getContext(), chatItems);

        ItemDividerDecorator decoration = new ItemDividerDecorator(getContext(), null, false, false);
        mRecylerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecylerView.addItemDecoration(decoration);
        mRecylerView.setAdapter(chatsRecyclerViewAdapter);

        //retrieving locally stored chat items
        getChatsFromCache();
        chatHandler();

        //retrieving locally stored group chat items
        getGroupsFromCache(getContext());
        groupHandler();

        return view;
    }

    public void chatHandler(){

        final Map<String, ChatItems> usersSnapshot = new HashMap<>();
        final String[] tempChatId = new String[1];

        //starting time messages thread
        TimeMessageThread mthread = new TimeMessageThread("MessageListener");
        mthread.start();

        chatsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
                ChatItems chat = dataSnapshot.getValue(ChatItems.class);

                HashMap<String, String> mids = new HashMap<>();
                mids.put(chat.getUserOne(), chat.getUserTwo());
                fireCheckers.put(mids, dataSnapshot.getKey());

                if(chat.getUserTwo().equals(currentUser.getUid().toString()) || chat.getUserOne().equals(currentUser.getUid().toString())){
                    tempChatId[0] = dataSnapshot.getKey();
                    usersSnapshot.put(dataSnapshot.getKey(), chat);

                    mDatabase.child("messages").child(dataSnapshot.getKey()).addChildEventListener(new ChildEventListener() {

                        @Override
                        public void onChildAdded(DataSnapshot snapshot, String s) {

                            MessageItems message = snapshot.getValue(MessageItems.class);
                            String lastMsg = snapshot.child("messageText").getValue().toString();
                            message.setMessageId(snapshot.getKey());

                            if(message.getDeliverTime() == null){
                                if(!usersSnapshot.get(dataSnapshot.getKey()).getUserOne().equals(currentUser.getUid())){
                                    setChatId(getContext(),
                                            usersSnapshot.get(dataSnapshot.getKey()).getUserOneName(),
                                            usersSnapshot.get(dataSnapshot.getKey()).getUserOne(),
                                            usersSnapshot.get(dataSnapshot.getKey()).getUserOne() + ".jpg",
                                            dataSnapshot.getKey().toString(),
                                            lastMsg);
                                }
                                else if(usersSnapshot.get(dataSnapshot.getKey()).getUserOne().equals(currentUser.getUid())){
                                    setChatId(getContext(),
                                            usersSnapshot.get(dataSnapshot.getKey()).getUserTwoName(),
                                            usersSnapshot.get(dataSnapshot.getKey()).getUserTwo(),
                                            usersSnapshot.get(dataSnapshot.getKey()).getUserTwo() + ".jpg",
                                            dataSnapshot.getKey().toString(),
                                            lastMsg);
                                }
                                if(message.getDestructTime() != null){
                                    Long time = (TabMain.getCurrentTimestamp() + Long.parseLong(message.getDestructTime()));
                                    message.setDestructTime(time.toString());
                                    timeMessages.add(new TimeMessages(dataSnapshot.getKey(), message));
                                }
                            }
                            else {
                                Long time = (TabMain.getCurrentTimestamp() + Long.parseLong(message.getDeliverTime()));
                                message.setDeliverTime(time.toString());
                                timeMessages.add(new TimeMessages(dataSnapshot.getKey(), message));
                            }

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot snapshot) {
                            if(!usersSnapshot.get(dataSnapshot.getKey()).getUserOne().equals(currentUser.getUid())){
                                setChatId(getContext(),
                                        usersSnapshot.get(dataSnapshot.getKey()).getUserOneName(),
                                        usersSnapshot.get(dataSnapshot.getKey()).getUserOne(),
                                        usersSnapshot.get(dataSnapshot.getKey()).getUserOne() + ".jpg",
                                        dataSnapshot.getKey().toString(),
                                        "");
                            }
                            else if(usersSnapshot.get(dataSnapshot.getKey()).getUserOne().equals(currentUser.getUid())){
                                setChatId(getContext(),
                                        usersSnapshot.get(dataSnapshot.getKey()).getUserTwoName(),
                                        usersSnapshot.get(dataSnapshot.getKey()).getUserTwo(),
                                        usersSnapshot.get(dataSnapshot.getKey()).getUserTwo() + ".jpg",
                                        dataSnapshot.getKey().toString(),
                                        "");
                            }
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        mDatabase.child("messages").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for(DataSnapshot chat : dataSnapshot.getChildren()){
//                    if(chat.getKey().equals(tempChatId[0])){
//
//                        String lastMsg = null;
//
//                        for(DataSnapshot lasts : chat.getChildren()){
//                            lastMsg = lasts.child("messageText").getValue().toString();
//                        }
//                        if(!usersSnapshot.get(chat.getKey()).getUserOne().equals(currentUser.getUid())){
//
//                            setChatId(getContext(),
//                                    usersSnapshot.get(chat.getKey()).getUserOneName(),
//                                    usersSnapshot.get(chat.getKey()).getUserOne(),
//                                    usersSnapshot.get(chat.getKey()).getUserOne() + ".jpg",
//                                    chat.getKey().toString(),
//                                    lastMsg);
//                        }
//                        else if(usersSnapshot.get(chat.getKey()).getUserOne().equals(currentUser.getUid())){
//                            setChatId(getContext(),
//                                    usersSnapshot.get(chat.getKey()).getUserTwoName(),
//                                    usersSnapshot.get(chat.getKey()).getUserTwo(),
//                                    usersSnapshot.get(chat.getKey()).getUserTwo() + ".jpg",
//                                    chat.getKey().toString(),
//                                    lastMsg);
//                        }
//                        break;
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
    }

    public static boolean newChat(Context context, String name, String number, String uid, String imageId){

        if(!checkers.contains(uid)){
            HashMap<String , String> mid1 = new HashMap<>();
            HashMap<String , String> mid2 = new HashMap<>();
            mid1.put(uid, currentUser.getUid());
            mid2.put(currentUser.getUid(), uid);
            if(!fireCheckers.isEmpty() && fireCheckers.containsKey(mid1)){
                setChatId(context, name, uid, imageId, fireCheckers.get(mid1), "");
            }
            else if(!fireCheckers.isEmpty() && fireCheckers.containsKey(mid2)){
                setChatId(context, name, uid, imageId, fireCheckers.get(mid2), "");
            }
            else{
                String chatId = setChatId(context, name, uid, imageId,  null, "");

                mDatabase.child("chats").child(chatId).setValue(new ChatItems(
                        currentUser.getDisplayName().toString(),
                        currentUser.getUid().toString(),
                        name,
                        imageId,
                        uid,
                        chatId));
            }

            return true;
        }
        return false;
    }

    public static String setChatId(Context context, String name,String contactUid, String imageId, String  fireChatId, String lastmsg){

        Gson gson = new Gson();
        String chatId = chatsRef.push().getKey().toString();
        ContactItems contact;

        if(fireChatId != null){
            contact = new ContactItems(name, lastmsg, fireChatId, imageId);
            chatId = fireChatId;
        }
        else {
            contact = new ContactItems(name, lastmsg, chatId, imageId);
        }
        if(!checkers.contains(contactUid)) {

            updateJsonFiles(context, chatId, "chats.json");

            String jsonString = IOClass.readFromFile(context.getApplicationContext(), "chats.json");
            Type chatTypeList = new TypeToken<ArrayList<ChatItems>>() {
            }.getType();
            List<ChatItems> chats = gson.fromJson(jsonString, chatTypeList);

            if (chats != null) {
                chats.add(new ChatItems(
                        currentUser.getDisplayName().toString(),
                        currentUser.getUid().toString(),
                        name,
                        imageId,
                        contactUid,
                        chatId));
            } else {
                chats = new ArrayList<>();
                chats.add(new ChatItems(
                        currentUser.getDisplayName().toString(),
                        currentUser.getUid().toString(),
                        name,
                        imageId,
                        contactUid,
                        chatId));
            }

            chatItems.add(0, contact);
            checkers.add(0, contactUid);

            String resultJson = gson.toJson(chats);
            IOClass.writeToFile(context.getApplicationContext(), "chats.json", resultJson);
        }
        else{

            chatItems.remove(checkers.indexOf(contactUid));
            checkers.remove(contactUid);

            chatItems.add(0, contact);
            checkers.add(0, contactUid);
        }

        chatsRecyclerViewAdapter.notifyDataSetChanged();

        return chatId;
    }

    public void getChatsFromCache(){
        Gson gson = new Gson();
        String jsonString = IOClass.readFromFile(getContext().getApplicationContext(), "chats.json");
        Type chatTypeList = new TypeToken<ArrayList<ChatItems>>(){}.getType();
        List<ChatItems> chats = gson.fromJson(jsonString, chatTypeList);
        if(chats != null){
            for (int i = 0; i < chats.size(); i++){
                if(chats.get(i).getUserOne().equals(currentUser.getUid())){
                    chatItems.add(0, new ContactItems(
                            chats.get(i).getUserTwoName(),
                            "last message",
                            chats.get(i).getChatId(),
                            chats.get(i).getUserTwoImageId()));

                    checkers.add(0, chats.get(i).getUserTwo());
                }
            }

        }
        chatsRecyclerViewAdapter.notifyDataSetChanged();
    }

    //group chat handling
    public void groupHandler(){

        groupsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                ChatItems group = dataSnapshot.getValue(ChatItems.class);
                if(group.getGroupMembers().containsKey(currentUser.getUid())){
                    setGroupChatId(
                            getContext(),
                            group.getGroupTitle(),
                            group.getGroupMembers(),
                            dataSnapshot.getKey(),
                            "",
                            group.getUserTwoImageId());

                    mDatabase.child("messages").child(dataSnapshot.getKey()).addChildEventListener(new ChildEventListener() {

                        @Override
                        public void onChildAdded(DataSnapshot snapshot, String s) {

                            MessageItems message = snapshot.getValue(MessageItems.class);
                            String lastMsg = snapshot.child("messageText").getValue().toString();
                            message.setMessageId(snapshot.getKey());

                            if(message.getDeliverTime() == null){

                                chatItems.get(checkers.indexOf(dataSnapshot.getKey())).setPhone(lastMsg);

                                if(message.getDestructTime() != null){
                                    Long time = (TabMain.getCurrentTimestamp() + Long.parseLong(message.getDestructTime()));
                                    message.setDestructTime(time.toString());
                                    timeMessages.add(new TimeMessages(dataSnapshot.getKey(), message));
                                }
                            }
                            else {
                                Long time = (TabMain.getCurrentTimestamp() + Long.parseLong(message.getDeliverTime()));
                                message.setDeliverTime(time.toString());
                                timeMessages.add(new TimeMessages(dataSnapshot.getKey(), message));
                            }

                            chatsRecyclerViewAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot snapshot) {
                            chatItems.get(checkers.indexOf(dataSnapshot.getKey())).setPhone("");
                            chatsRecyclerViewAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                ChatItems group = dataSnapshot.getValue(ChatItems.class);
                if(Objects.requireNonNull(group).getGroupMembers().containsKey(currentUser.getUid())) {
                    chatItems.get(checkers.indexOf(dataSnapshot.getKey())).setName(dataSnapshot.child("groupTitle").getValue().toString());
                    IOClass.downloadImage(Objects.requireNonNull(getContext()).getApplicationContext(), chatItems.get(checkers.indexOf(dataSnapshot.getKey())).getImageId());

                    updateJsonFiles(getContext(), dataSnapshot.getKey(), "groups.json");

                    String jsonString = IOClass.readFromFile(getContext().getApplicationContext(), "groups.json");
                    Type chatTypeList = new TypeToken<ArrayList<ChatItems>>() {
                    }.getType();
                    Gson gson = new Gson();
                    List<ChatItems> groups = gson.fromJson(jsonString, chatTypeList);
                    groups.add(group);
                    String resultJson = gson.toJson(groups);
                    IOClass.writeToFile(getContext().getApplicationContext(), "groups.json", resultJson);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static String newGroupChat(Context context, String groupTitle, HashMap<String, String> groupMembers, String lastMessage, String imageId){

        String groupChatId = setGroupChatId(context, groupTitle, groupMembers, null, "", imageId);

        groupsRef.child(groupChatId).setValue(new ChatItems(
                groupTitle,
                groupMembers,
                groupChatId,
                imageId));

        return groupChatId;
    }

    public static String setGroupChatId(Context context, String groupTitle, HashMap<String, String> groupMembers, String fireId, String lastMessage, String imageId){
        Gson gson = new Gson();
        String groupChatId = groupsRef.push().getKey();
        ContactItems contact;

        if(fireId != null){
            contact = new ContactItems(groupTitle, lastMessage, fireId, imageId);
            groupChatId = fireId;
        }
        else {
            contact = new ContactItems(groupTitle, lastMessage, groupChatId, imageId);
        }

        if(!checkers.contains(groupChatId)){

            String jsonString = IOClass.readFromFile(context.getApplicationContext(), "groups.json");
            Type chatTypeList = new TypeToken<ArrayList<ChatItems>>() {
            }.getType();
            List<ChatItems> groups = gson.fromJson(jsonString, chatTypeList);

            if (groups != null) {
                groups.add(new ChatItems(
                        groupTitle,
                        groupMembers,
                        groupChatId,
                        imageId));
            } else {
                groups = new ArrayList<>();
                groups.add(new ChatItems(
                        groupTitle,
                        groupMembers,
                        groupChatId,
                        imageId));
            }

            checkers.add(0, groupChatId);

            contact.setGroup(true);
            chatItems.add(0, contact);

            String resultJson = gson.toJson(groups);
            IOClass.writeToFile(context.getApplicationContext(), "groups.json", resultJson);
        }
        else {
            chatItems.remove(checkers.indexOf(groupChatId));
            checkers.remove(groupChatId);

            contact.setGroup(true);
            chatItems.add(0, contact);
            checkers.add(0, groupChatId);
        }

        chatsRecyclerViewAdapter.notifyDataSetChanged();

        return groupChatId;
    }

    private static void getGroupsFromCache(Context context){
        Gson gson = new Gson();
        String jsonString = IOClass.readFromFile(context.getApplicationContext(), "groups.json");
        Type groupTypeList = new TypeToken<ArrayList<ChatItems>>(){}.getType();
        List<ChatItems> groups = gson.fromJson(jsonString, groupTypeList);
        if(groups != null){
            for (int i = 0; i < groups.size(); i++){
                if(groups.get(i).getGroupMembers().containsKey(currentUser.getUid())){
                    ChatsFragment.chatItems.add(0, new ContactItems(
                            groups.get(i).getGroupTitle(),
                            "",
                            groups.get(i).getGroupId(),
                            groups.get(i).getUserTwoImageId(),
                            true
                    ));

                    checkers.add(0, groups.get(i).getGroupId());
                }
            }
        }
        ChatsFragment.chatsRecyclerViewAdapter.notifyDataSetChanged();
    }

    public static void updateJsonFiles(Context context, String targetUid, String fileName){
        Gson gson = new Gson();
        String jsonString = IOClass.readFromFile(context.getApplicationContext(), fileName);
        Type chatTypeList = new TypeToken<ArrayList<ChatItems>>() {
        }.getType();
        List<ChatItems> items = gson.fromJson(jsonString, chatTypeList);

        if(items != null){
            for (int i = 0; i < items.size(); i++){
                if(fileName.equals("chats.json")){
                    if (items.get(i).getChatId().equals(targetUid)){
                        items.remove(i);
                    }
                }
                else if(fileName.equals("groups.json")){
                    if (items.get(i).getGroupId().equals(targetUid)){
                        items.remove(i);
                    }
                }
            }
        }
        String resultJson = gson.toJson(items);
        IOClass.writeToFile(context.getApplicationContext(), fileName, resultJson);

    }

    class TimeMessageThread extends Thread {
        private Thread t;
        private String threadName;

        TimeMessageThread(String threadName) {
            this.threadName = threadName;
        }

        public void run() {
           while (true){
               try {

                   long currentTime = TabMain.getCurrentTimestamp();

                   if(!timeMessages.isEmpty()){
                       for(TimeMessages message : timeMessages){
                           if(!message.getMessageItems().getSenderId().equals(currentUser.getUid())){
                               if(message.getMessageItems().getDeliverTime() != null){
                                   if(Long.parseLong(message.getMessageItems().getDeliverTime()) <= currentTime){

                                       message.getMessageItems().setDeliverTime(null);
                                       mDatabase
                                               .child("messages")
                                               .child(message.getId()) //chat level
                                               .child(message.getMessageItems().getMessageId()) //message level
                                               .removeValue();
                                       mDatabase
                                               .child("messages")
                                               .child(message.getId()) //chat level
                                               .child(message.getMessageItems().getMessageId()) //message level
                                               .setValue(message.getMessageItems());

                                   }
                               }
                               if(message.getMessageItems().getDestructTime() != null){
                                   if(Long.parseLong(message.getMessageItems().getDestructTime()) <= currentTime){
                                       message.getMessageItems().setDestructTime(null);
                                       mDatabase
                                               .child("messages")
                                               .child(message.getId()) //chat level
                                               .child(message.getMessageItems().getMessageId()) //message level
                                               .removeValue();
                                   }
                               }
                           }
                       }
                   }

                   Thread.sleep(10000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        }

        public void start () {
            System.out.println("Starting " +  threadName );
            if (t == null) {
                t = new Thread (this, threadName);
                t.start ();
            }
        }
    }

}
