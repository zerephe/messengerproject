package ekubekn.android.whattx.tab_fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.ImageWriter;
import ekubekn.android.whattx.R;
import ekubekn.android.whattx.group_chat.GroupFragment;
import ekubekn.android.whattx.single_chat.MessagesFragment;


public class ChatsRecyclerViewAdapter extends RecyclerView.Adapter<ChatsRecyclerViewAdapter.ViewHolder> {

    private Dialog contactDialog;
    private Context mContext;
    private List<ContactItems> mContactData;

    public ChatsRecyclerViewAdapter(Context mContext, List<ContactItems> mContactData) {
        this.mContext = mContext;
        this.mContactData = mContactData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.item_chats, parent, false);
        final ViewHolder myViewHolder = new ViewHolder(view);

        myViewHolder.chats_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newChat;
                if(mContactData.get(myViewHolder.getAdapterPosition()).isGroup()){
                    newChat = new Intent(mContext, GroupFragment.class);
                }
                else{
                    newChat = new Intent(mContext, MessagesFragment.class);
                }
                newChat.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                newChat.putExtra("chatID", mContactData.get(myViewHolder.getAdapterPosition()).getuID());
                newChat.putExtra("userTwo",  mContactData.get(myViewHolder.getAdapterPosition()).getName());
                mContext.startActivity(newChat);
            }
        });

        myViewHolder.chats_item_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //deleting chat items: chats, groups
                if(mContactData.get(myViewHolder.getAdapterPosition()).isGroup()){
                    ChatsFragment.groupsRef
                            .child(mContactData.get(myViewHolder.getAdapterPosition()).getuID())
                            .child("groupMembers")
                            .child(ChatsFragment.currentUser.getUid()).setValue(null);

                    ChatsFragment.updateJsonFiles(
                            mContext,
                            mContactData.get(myViewHolder.getAdapterPosition()).getuID(),
                            "groups.json");

                    ChatsFragment.checkers.remove(mContactData.get(myViewHolder.getAdapterPosition()).getuID());
                    ChatsFragment.chatItems.remove(myViewHolder.getAdapterPosition());
                }
                else{
                    ChatsFragment.updateJsonFiles(
                            mContext,
                            mContactData.get(myViewHolder.getAdapterPosition()).getuID(),
                            "chats.json");

                    ChatsFragment.checkers.remove(myViewHolder.getAdapterPosition());
                    ChatsFragment.chatItems.remove(myViewHolder.getAdapterPosition());
                }

                notifyDataSetChanged();
                return true;
            }
        });

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.contactName.setText(mContactData.get(position).getName());
        holder.lastMessage.setText(mContactData.get(position).getPhone());

        if(mContactData.get(holder.getAdapterPosition()).getImageId() != null && !mContactData.get(holder.getAdapterPosition()).getImageId().equals("null.jpg")){
            if(!new File(mContext.getFilesDir(), mContactData.get(holder.getAdapterPosition()).getImageId()).exists()){
                DownLoadImageThread thread = new DownLoadImageThread(
                        "Thread Chat",
                        holder,
                        mContactData.get(holder.getAdapterPosition()).getImageId());
                thread.start();
            }

            Picasso.get()
                    .load(new File(mContext.getFilesDir(), mContactData.get(holder.getAdapterPosition()).getImageId()))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(80, 80)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                    .into(holder.contactImage);
        }
        else{
            holder.contactImage.setImageResource(R.drawable.ic_profile_image_sample_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return mContactData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout chats_item_layout;
        private TextView contactName;
        private TextView lastMessage;
        private ImageView contactImage;

        public ViewHolder(View view){
            super(view);
            chats_item_layout = view.findViewById(R.id.chats_item_layout);
            contactName = view.findViewById(R.id.contact_name);
            lastMessage = view.findViewById(R.id.last_message);
            contactImage = (ImageView) view.findViewById(R.id.contact_image);
        }
    }

    class DownLoadImageThread extends Thread {
        private Thread t;
        private String threadName, position;
        private ViewHolder holder;

        DownLoadImageThread(String name, ViewHolder holder, String position) {
            this.holder = holder;
            threadName = name;
            this.position = position;
        }

        public void run() {
            int i = 0;
            while (!IOClass.callback || i < 20) {
                try {
                    i++;
                    IOClass.downloadImage(mContext, position);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            IOClass.callback = false;
        }

        public void start () {
            System.out.println("Starting " +  threadName );
            if (t == null) {
                t = new Thread (this, threadName);
                t.start ();
            }
        }
    }
}
