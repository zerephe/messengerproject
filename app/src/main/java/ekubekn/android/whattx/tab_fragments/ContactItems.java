package ekubekn.android.whattx.tab_fragments;

/**
 * Created by Bekn on 22-Mar-18.
 */

public class ContactItems {

    private String name;
    private String phone;
    private String uID;
    private String imageId;
    private boolean group = false;

    public ContactItems() {
    }

    public ContactItems(String name, String phone, String uID, String image) {
        this.name = name;
        this.phone = phone;
        this.imageId = image;
        this.uID = uID;
    }
    public ContactItems(String name, String phone, String uID, String image, boolean group) {
        this.name = name;
        this.phone = phone;
        this.imageId = image;
        this.uID = uID;
        this.group = group;
    }

    //getters

    public boolean isGroup() {
        return group;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getImageId() {
        return imageId;
    }

    public String getuID() {
        return uID;
    }

    //setters

    public void setGroup(boolean group) {
        this.group = group;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}
