package ekubekn.android.whattx.tab_fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.ItemDividerDecorator;
import ekubekn.android.whattx.PhoneVerification;
import ekubekn.android.whattx.R;
import ekubekn.android.whattx.TabMain;

/**
 * Created by Bekn on 22-Mar-18.
 */

public class ContactsFragment extends Fragment {

    private static RecyclerView mRecylerView;
    public static ContactsRecyclerViewAdapter contactsRecyclerViewAdapter;
    public static List<ContactItems> contactItems;
    public static List<String> phoneNumbers = new ArrayList<>();;
    public static String currentUserImageId = null;

    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase, userRef;

    View view;

    public ContactsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_contacts, container, false);
        mRecylerView = view.findViewById(R.id.contact_recycler_view);

        ItemDividerDecorator decoration = new ItemDividerDecorator(getContext(), null, false, false);
        refreshContactRecycler(getContext());
        mRecylerView.addItemDecoration(decoration);

        if(PhoneVerification.getContact){
            getPhoneContacts();
        }
        getContacts();

        return view;
    }

    public static void refreshContactRecycler(Context context){
        contactsRecyclerViewAdapter = new ContactsRecyclerViewAdapter(context, contactItems);
        mRecylerView.setLayoutManager(new LinearLayoutManager(context));
        mRecylerView.setAdapter(contactsRecyclerViewAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        userRef = mDatabase.child("users");

        contactItems = new ArrayList<>();

    }



    public void getContacts(){

        final Gson gson = new Gson();
        String contactString = IOClass.readFromFile(Objects.requireNonNull(getContext().getApplicationContext()), "contacts.json");
        Type contactTypeList = new TypeToken<ArrayList<ContactItems>>(){}.getType();
        List<ContactItems> contacts = gson.fromJson(contactString, contactTypeList);

        if(contactString != null && contacts != null){
            contactItems.clear();
            contactItems.addAll(contacts);
        }
        else{
            contacts = new ArrayList<>();
        }

        final List<ContactItems> finalContacts = new ArrayList<>();

        userRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                int i = 0;

                if(dataSnapshot.getKey().equals(currentUser.getUid())){
                    if(dataSnapshot.child("profileImage").getValue() != null && !dataSnapshot.child("profileImage").getValue().toString().equals("null.jpg")){
                        currentUserImageId = dataSnapshot.child("profileImage").getValue().toString();
                    }
                }

                if(!phoneNumbers.isEmpty()){
                    while(i < phoneNumbers.size()){

                        if(dataSnapshot.child("phoneNumber").getValue().toString().equals(phoneNumbers.get(i))){
                            if(!phoneNumbers.get(i).equals(currentUser.getPhoneNumber().toString())){

                                String imageId = null;

                                if(dataSnapshot.child("profileImage").getValue() != null && !dataSnapshot.child("profileImage").getValue().toString().equals("null.jpg")){
                                    imageId = dataSnapshot.child("profileImage").getValue().toString();
                                }

                                finalContacts.add(new ContactItems(
                                        dataSnapshot.child("username").getValue().toString(),
                                        dataSnapshot.child("phoneNumber").getValue().toString(),
                                        dataSnapshot.getKey().toString(),
                                        imageId));
                            }
                            break;
                        }
                        i++;
                    }
                }
                contactItems.clear();
                contactItems.addAll(finalContacts);
                String resultJson = gson.toJson(finalContacts);
                IOClass.writeToFile(getContext().getApplicationContext(), "contacts.json", resultJson);
                contactsRecyclerViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                String jsonString = IOClass.readFromFile(getContext().getApplicationContext(), "chats.json");
                Type chatTypeList = new TypeToken<ArrayList<ChatItems>>() {
                }.getType();
                List<ChatItems> chats = gson.fromJson(jsonString, chatTypeList);

                for(ChatItems chat: chats){
                    if(chat.getChatId().equals(ChatsFragment.fireCheckers.get(dataSnapshot.getKey()))){

                        if(chat.getUserOne().equals(dataSnapshot.getKey())){
                            chat.setUserOneName(dataSnapshot.child("username").getValue().toString());
                        }
                        else if(chat.getUserTwo().equals(dataSnapshot.getKey())){
                            ChatsFragment.chatItems.remove(ChatsFragment.checkers.indexOf(dataSnapshot.getKey()));
                            ChatsFragment.checkers.remove(dataSnapshot.getKey());
                            chat.setUserTwoName(dataSnapshot.child("username").getValue().toString());
                        }
                        mDatabase.child("chats").child(ChatsFragment.fireCheckers.get(dataSnapshot.getKey())).removeValue();
                        mDatabase.child("chats").child(ChatsFragment.fireCheckers.get(dataSnapshot.getKey())).setValue(chat);
                        break;
                    }
                }

                IOClass.downloadImage(getContext().getApplicationContext(), dataSnapshot.getKey() + ".jpg");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                String removedContact = dataSnapshot.getKey();
                for(int i = 0; i < contactItems.size(); i++){
                    if(contactItems.get(i).getuID().equals(removedContact)){
                        contactItems.remove(i);
                    }
                }

                String resultJson = gson.toJson(contactItems);
                IOClass.writeToFile(getContext().getApplicationContext(), "contacts.json", resultJson);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //retrieving address book
    public void getPhoneContacts(){
        //<<<<<<<<<<<<<<<<<<<CONTACTS Retrieving!!!!!
        ContentResolver contentResolver = getContext().getContentResolver();
        Cursor contactCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        while (contactCursor.moveToNext()){
            String id = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts._ID));
            String name = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            //int hasPhoneNumber = Integer.parseInt(contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));

            Cursor phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{id},
                    null);

            while (phoneCursor.moveToNext()){
                String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                phoneNumber = phoneNumber.replaceAll(" ", "");
                phoneNumbers.add(phoneNumber);
            }
            phoneCursor.close();

        }
        contactCursor.close();
    }

}
