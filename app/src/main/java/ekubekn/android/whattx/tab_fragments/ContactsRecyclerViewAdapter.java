package ekubekn.android.whattx.tab_fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import ekubekn.android.whattx.IOClass;
import ekubekn.android.whattx.ImageViewer;
import ekubekn.android.whattx.NewGroupChatActivity;
import ekubekn.android.whattx.R;
import ekubekn.android.whattx.TabMain;
import ekubekn.android.whattx.single_chat.MessagesFragment;


public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.ViewHolder> {

    private Dialog contactDialog;
    public static Context mContext, m2Context;
    private List<ContactItems> mContactData;
    public static HashMap<String, String> mUserSelector = new HashMap<>();
    static View view;

    public static boolean selectUser = false;
    private boolean groupSettings = false;

    public Button dWriteMessage;

    public ContactsRecyclerViewAdapter(Context mContext, List<ContactItems> mContactData) {
        this.mContext = mContext;
        this.mContactData = mContactData;
    }

    public ContactsRecyclerViewAdapter(Context mContext, List<ContactItems> mContactData, boolean groupSettings){
        this.m2Context = mContext;
        this.mContactData = mContactData;
        this.groupSettings = groupSettings;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(mContext).inflate(R.layout.item_contact, parent, false);
        final ViewHolder myViewHolder = new ViewHolder(view);

        if(groupSettings){
            contactDialog = new Dialog(m2Context);
        }
        else{
            contactDialog = new Dialog(mContext);
        }
        contactDialog.setContentView(R.layout.contact_dialog_window);
        contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dWriteMessage = contactDialog.findViewById(R.id.write_message_button);

        myViewHolder.contact_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
    //          Toast.makeText(mContext, "test lcik" + String.valueOf(myViewHolder.getAdapterPosition()), Toast.LENGTH_SHORT).show();

                if(selectUser){
                    if(!mUserSelector.containsKey(mContactData.get(myViewHolder.getAdapterPosition()).getuID())){
                        mUserSelector.put(
                                mContactData.get(myViewHolder.getAdapterPosition()).getuID(),
                                mContactData.get(myViewHolder.getAdapterPosition()).getName());

                        myViewHolder.contact_item_layout.setBackgroundColor(mContext.getResources().getColor(R.color.hint_color));
                    }
                    else {
                        mUserSelector.remove(mContactData.get(myViewHolder.getAdapterPosition()).getuID());
                        myViewHolder.contact_item_layout.setBackgroundColor(mContext.getResources().getColor(R.color.main_color));
                    }

                }
                else {
                    TextView dContactName = contactDialog.findViewById(R.id.dialog_contact_name);
                    TextView dContactPhone = contactDialog.findViewById(R.id.dialog_contact_phone);
                    ImageView dContactImage = contactDialog.findViewById(R.id.dialog_contact_image);
                    dContactName.setText(mContactData.get(myViewHolder.getAdapterPosition()).getName());
                    dContactPhone.setText(mContactData.get(myViewHolder.getAdapterPosition()).getPhone());

                    dContactImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            IOClass.downloadImage(mContext.getApplicationContext(), mContactData.get(myViewHolder.getAdapterPosition()).getImageId());
                            Intent imageViewer = new Intent(mContext, ImageViewer.class);
                            imageViewer.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                            imageViewer.putExtra("imageId", mContactData.get(myViewHolder.getAdapterPosition()).getImageId());
                            mContext.startActivity(imageViewer);
                        }
                    });

                    if(mContactData.get(myViewHolder.getAdapterPosition()).getImageId() != null){
                        Picasso.get()
                                .load(new File(mContext.getFilesDir(), mContactData.get(myViewHolder.getAdapterPosition()).getImageId()))
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .resize(300, 300)
                                .centerCrop()
                                .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                                .into(dContactImage);
                    }
                    else {
                        dContactImage.setImageResource(R.drawable.ic_image_sample_190dp);
                    }

                    contactDialog.show();

                    dWriteMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ChatsFragment.newChat(
                                    mContext,
                                    mContactData.get(myViewHolder.getAdapterPosition()).getName(),
                                    "",
                                    mContactData.get(myViewHolder.getAdapterPosition()).getuID(),
                                    mContactData.get(myViewHolder.getAdapterPosition()).getImageId());

                            Intent newChat = new Intent(mContext, MessagesFragment.class);
                            newChat.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                            newChat.putExtra("userTwo", mContactData.get(myViewHolder.getAdapterPosition()).getuID());
                            mContext.startActivity(newChat);

                            contactDialog.hide();
                        }
                    });
                }

            }
        });

        myViewHolder.contact_item_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(!groupSettings){//checking if were not in group settings
                    if(!selectUser){
                        mUserSelector.put(
                                mContactData.get(myViewHolder.getAdapterPosition()).getuID(),
                                mContactData.get(myViewHolder.getAdapterPosition()).getName());

                        selectUser = true;
                        myViewHolder.contact_item_layout.setBackgroundColor(mContext.getResources().getColor(R.color.hint_color));
                        TabMain.newGroupButton.setVisibility(View.VISIBLE);

                        TabMain.newGroupButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(mUserSelector != null  && !mUserSelector.isEmpty()){
                                    Intent newGroup = new Intent(mContext, NewGroupChatActivity.class);
                                    mContext.startActivity(newGroup);

                                }
                            }
                        });

                    }
                    else{
                        mUserSelector.remove(mContactData.get(myViewHolder.getAdapterPosition()).getuID());
                    }
                }

                return true;
            }
        });


        return myViewHolder;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.contactName.setText(mContactData.get(position).getName());
        holder.contactPhone.setText(mContactData.get(position).getPhone());
        if(mContactData.get(holder.getAdapterPosition()).getImageId() != null){

            if(!new File(mContext.getFilesDir(), mContactData.get(holder.getAdapterPosition()).getImageId()).exists()){
                DownLoadImageThread thread = new DownLoadImageThread("Thread 1", holder);
                thread.start();
            }

            Picasso.get()
                    .load(new File(mContext.getFilesDir(), mContactData.get(holder.getAdapterPosition()).getImageId()))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(80, 80)
                    .centerCrop()
                    .onlyScaleDown() // the image will only be resized if it's bigger than 600x600 pixels
                    .into(holder.contactImage);
        }
        else {
            holder.contactImage.setImageResource(R.drawable.ic_profile_image_sample_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return mContactData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout contact_item_layout;
        private TextView contactName;
        private TextView contactPhone;
        private ImageView contactImage;

        public ViewHolder(View view){
            super(view);
            contact_item_layout = view.findViewById(R.id.contact_item_layout);
            contactName = view.findViewById(R.id.contact_name);
            contactPhone = view.findViewById(R.id.contact_phone);
            contactImage = (ImageView) view.findViewById(R.id.contact_image);
        }
    }

    public static void deselectAllContacts(){
        mUserSelector.clear();
        ContactsFragment.refreshContactRecycler(mContext);
        selectUser = false;
    }

    class DownLoadImageThread extends Thread {
        private Thread t;
        private String threadName;
        private ViewHolder holder;

        DownLoadImageThread(String name, ViewHolder holder) {
            this.holder = holder;
            threadName = name;
        }

        public void run() {
            while (!IOClass.callback){
                try {
                    IOClass.downloadImage(mContext, mContactData.get(holder.getAdapterPosition()).getImageId());
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            IOClass.callback = false;
        }

        public void start () {
            System.out.println("Starting " +  threadName );
            if (t == null) {
                t = new Thread (this, threadName);
                t.start ();
            }
        }
    }
}
